#!/usr/bin/env python3
"""
Module Docstring
"""
__author__ = "Asraful islam"
__version__ = "0.1.0"
__license__ = "MIT"

import whois
import os
import json
from datetime import datetime
import time
from filefolder import FileFolder 

domain_list = ['blah','https://www.firstprincipium.com', 'https://www.pragmaticxr.com', 'https://www.magicleap.com',
                'https://www.thepragmaticai.com', 'https://www.a3ir.com', 'https://www.kupanu.com']

def read_file_into_list(file_path):
    """
    Read the contents of a file into a list.
    
    Args:
        file_path (str): Path to the file.
    
    Returns:
        list: A list containing the lines of the file.
    """
    try:
        with open(file_path, 'r') as file:
            lines = file.readlines()            
            lines = [line.strip() for line in lines]
            return lines
    except FileNotFoundError:
        print(f"File '{file_path}' not found.")
        return []

# Function to handle datetime objects
def datetime_handler(obj):
    if isinstance(obj, datetime):
        return obj.isoformat()
    else:
        raise TypeError("Type %s not serializable" % type(obj))

def create_filename_from_domain(domain):
        file_name = ""
        domain_in_list = domain.split('.')
        if len(domain_in_list)>1: 
            file_name = f'{domain_in_list[-2]}.{domain_in_list[-1]}.json'
        
        return(file_name)

def save_whois_info(domain_name,output_folder):    
    file_name = create_filename_from_domain(domain_name)
    if file_name != "":
        try:
            print(f"getting domain info for: {domain_name}") 
            whois_info = whois.whois(domain_name)
            json_data =json.dumps(whois_info,default=datetime_handler)
            output_path = os.path.join(output_folder,file_name)
            with open(output_path, 'w') as json_file:
                json.dump(json_data, json_file)
        except: 
            print(f"failed: {domain_name}")

def main():
    """ Main entry point of the app """
    file_path = f'/Users/ai/repos/gitlab/domain-management/src/big_list.txt'
    file_path = f'/Users/ai/repos/gitlab/domain-management/src/domain_list.txt'
    domain_list = read_file_into_list(file_path)
    ff = FileFolder()    
    output_folder = (ff.create_all_output_folders(__file__))
    for domain in domain_list:     
        save_whois_info(domain,output_folder)
        time.sleep(1)

if __name__ == "__main__":
    """ This is executed when run from the command line """
    main()
 