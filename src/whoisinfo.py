from datetime import datetime

class WhoisInfo:

    def __init__(self,json_object):
        self.whois_info = json_object
        self.whois_info_dict = dict()
        self.extract_info()    

    def extract_info(self):
        self.__get_domain_name()
        self.__get_registrar()
        self.__get_name_servers()
        self.__get_expiration_date()
        self.__get_creation_date()
        self.__get_updated_date()
        
        
        # extract_item(whois_info['updated_date'])

    def __get_domain_name(self):
        if isinstance(self.whois_info['domain_name'], list):
            domain_name = self.whois_info['domain_name'][0]
        else:
            domain_name = self.whois_info['domain_name']
        
        self.whois_info_dict['domain_name'] = domain_name.lower()
    
    def __get_name_servers(self):
        if isinstance(self.whois_info['name_servers'], list):
            name_servers = ','.join(self.whois_info['name_servers']) 
        else:
            name_servers = self.whois_info['name_servers']
        
        self.whois_info_dict['name_servers'] = name_servers.lower()
    
    def __get_registrar(self):        
        self.whois_info_dict['registrar'] = self.whois_info['registrar'].lower()
    
    def __get_expiration_date(self):
        exp_date = self.__convert_date_to_string(self.whois_info['expiration_date'])
        self.whois_info_dict['expiration_date'] = exp_date

    def __get_creation_date(self):
        creation_date = self.__convert_date_to_string(self.whois_info['creation_date'])
        self.whois_info_dict['creation_date'] = creation_date

    def __get_updated_date(self):
        updated_date = self.__convert_date_to_string(self.whois_info['updated_date'])
        self.whois_info_dict['updated_date'] = updated_date


    def __convert_date_to_string(self,date):
        if isinstance(date, list):
            exp_date = date[0]            
        elif isinstance(date, datetime):
            exp_date = date
        exp_date_string  = exp_date.strftime("%Y-%m-%d")
        return(exp_date_string)