#!/usr/bin/env python3
"""
Module Docstring
"""
__author__ = "Asraful islam"
__version__ = "0.1.0"
__license__ = "MIT"

import whois
from datetime import datetime
import filefolder
import whoisinfo
import time
import logging
from datetime import datetime


def get_logger(fname=""):
    if fname == "":
        fname = datetime.now().strftime("%Y_%m_%d")+".log"
    logging.basicConfig(filename=fname, format='%(asctime)s - %(levelname)s - %(message)s', level=logging.DEBUG)
    return logging


domain_list = ['blah','https://www.firstprincipium.com', 'https://www.pragmaticxr.com', 'https://www.magicleap.com',
                'https://www.thepragmaticai.com', 'https://www.a3ir.com', 'https://www.kupanu.com']

def read_file_into_list(file_path):
    """
    Read the contents of a file into a list.
    
    Args:
        file_path (str): Path to the file.
    
    Returns:
        list: A list containing the lines of the file.
    """
    try:
        with open(file_path, 'r') as file:
            lines = file.readlines()            
            lines = [line.strip() for line in lines]
            return lines
    except FileNotFoundError:
        print(f"File '{file_path}' not found.")
        return []

# Function to handle datetime objects
def datetime_handler(obj):
    if isinstance(obj, datetime):
        return obj.isoformat()
    else:
        raise TypeError("Type %s not serializable" % type(obj))

# def get_filename_from_domain(domain):
#         file_name = ""
#         domain_in_list = domain.split('.')
#         if len(domain_in_list)>1: 
#             file_name = f'{domain_in_list[-2]}.{domain_in_list[-1]}.json'
        
#         return(file_name)

def save_whois_info(domain_name):
    logger = get_logger() 
    try:
        logging.info(f"getting domain info for: {domain_name}") 
        print(f"getting domain info for: {domain_name}") 
        whois_info = whois.whois(domain_name)
        wi = whoisinfo.WhoisInfo(whois_info)
        logging.info(f"Domain name: {wi.whois_info_dict['domain_name']}")
        logging.info(f"Name Servers: {wi.whois_info_dict['name_servers']}")
        logging.info(f"Registrar: {wi.whois_info_dict['registrar']}")
        logging.info(f"Expiration Date: {wi.whois_info_dict['expiration_date']}")
        logging.info(f"Creation Date: {wi.whois_info_dict['creation_date']}")
        logging.info(f"Updated Date: {wi.whois_info_dict['updated_date']}")
        
        logging.info("+++++++++++++++++++++++++++++++++++++")

    except: 
        logging.error(f"failed: {domain_name}")
        logging.info(f"failed: {domain_name}")

def main():
    """ Main entry point of the app """        
    file_path = f'/Users/ai/repos/gitlab/domain-management/src/big_list.txt'
    #file_path = f'/Users/ai/repos/gitlab/domain-management/src/domain_list.txt'
    domain_list = read_file_into_list(file_path)
    print("running....")
    for domain in domain_list:        
        save_whois_info(domain)
        time.sleep(10)
    print("Done!")

if __name__ == "__main__":
    """ This is executed when run from the command line """
    main()