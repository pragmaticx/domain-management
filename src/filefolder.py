from datetime import datetime
import os 

class FileFolder:

    def __init__(self):        
        self.now = datetime.now() # current date and time
        self.year = self.now.strftime("%Y")
        self.month = self.now.strftime("%m")
        self.day = self.now.strftime("%d")

    def get_this_year(self):
        return str(self.year)

    def get_this_month(self):        
        this_month = f"{self.year}_{self.month}" 
        return this_month
    
    def get_this_day(self):        
        this_day = f"{self.year}_{self.month}_{self.day}" 
        return(this_day)

    def create_folder(self,folder_path):        
        if os.path.exists(folder_path):
            pass
        else:
            os.makedirs(folder_path)

    def create_output_folder_root(self,filepath):
        self.output_folder_root = os.path.join(os.path.dirname(filepath), "output")
        self.create_folder(self.output_folder_root)
        return self.output_folder_root
        
    def create_all_output_folders(self,filepath):
        self.output_folder_root = os.path.join(os.path.dirname(filepath), "output")
        self.create_folder(self.output_folder_root)

        year_path = os.path.join(self.output_folder_root,self.get_this_year())
        month_path = os.path.join(year_path,self.get_this_month())
        day_path =os.path.join(month_path,self.get_this_day())
        self.create_folder(year_path)
        self.create_folder(month_path)
        self.create_folder(day_path)
        return (day_path)

    