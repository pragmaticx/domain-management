+++
title = "1. Introduction"
date = 2023-12-27T20:07:25-08:00
weight = 1

+++
This is a step by step guide to build an app that will help monitor domains for basic whois related data.  
The end goal is to build a scalable solution to deploy in a cloud service i.e. gcp,aws.  
This will also going to be a demonestration of some technologics and platfomr that can be used for other related solutions.  

{{% children  %}}